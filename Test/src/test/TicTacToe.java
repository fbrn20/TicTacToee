package test;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.io.*;
import java.util.*;

//Änderung 21.04.16 17:37

public class TicTacToe {

	// Globals

	int field[] = new int[9];
	// JButton [] Felder = new JButton [9];

	void fillGrid() {

		for (int i = 0; i < field.length; i++) {
			field[i] = i;
		}
	}

	public void Steinelegen() {

		int Spieler1 = 'X';
		int Spieler2 = 'O';
		int zug;
		
		Scanner sc = new Scanner(System.in);
		
		while(true){
		System.out.print("Spieler 1 setze X: ");
		zug = sc.nextInt();
		System.out.println(zug);
		System.out.print("Spieler 2 setze O: ");
		zug = sc.nextInt();  
		
		}
	}

	// Zeichnet das Spielfeld auf der Konsole

	public String toString() {

		String s = field[0] + "|" + field[1] + "|" + field[2] + "\n" +
					"_____"  + "\n" +
					field[3] + "|" + field[4] + "|" + field[5] + "\n" +
					"_____" + "\n" +
					field[6] + "|" + field[7] + "|" + field[8];

		return s;
	}

	// Prüft ob es einen Gewinner gibt
	public int win() {

		for (int i = 0; i < field.length; i++) {

			if (field[0] + field[1] + field[2] == 'X') { // Reihe
				System.out.println("Spieler 1 gewinnt!");
				return field[0];
			}

			if (field[3] + field[4] + field[5] == 'X') { // Reihe
				System.out.println("Spieler 1 gewinnt!");
				return field[3];
			}

			if (field[6] + field[7] + field[8] == 'X') { // Reihe
				System.out.println("Spieler 1 gewinnt!");
				return field[6];
			}

			if (field[0] + field[3] + field[6] == 'X') { // Spalte
				System.out.println("Spieler 1 gewinnt!");
				return field[0];
			}

			if (field[1] + field[4] + field[7] == 'X') { // Spalte
				System.out.println("Spieler 1 gewinnt!");
				return field[1];
			}

			if (field[2] + field[5] + field[8] == 'X') { // Spalte

				System.out.println("Spieler 1 gewinnt!");
				return field[2];
			}

			if (field[0] + field[4] + field[8] == 'X') { // Diagonal
				System.out.println("Spieler 1 gewinnt");
				return field[0];
			}

			if (field[2] + field[4] + field[6] == 'X') { // Diagonal
				System.out.println("Spieler 1 gewinnt");
				return field[2];
			}

			if (field[0] + field[1] + field[2] == 'O') { // Reihe
				System.out.println("Spieler 2 gewinnt!");
				return field[0];

			}

			if (field[3] + field[4] + field[5] == -'O') { // Reihe
				System.out.println("Spieler 2 gewinnt!");
				return field[3];
			}

			if (field[6] + field[7] + field[8] == 'O') { // Reihe
				System.out.println("Spieler 2 gewinnt!");
				return field[6];
			}

			if (field[0] + field[3] + field[6] == 'O') { // Spalte
				System.out.println("Spieler 2 gewinnt!");
				return field[0];
			}

			if (field[1] + field[4] + field[7] == 'O') { // Spalte

				System.out.println("Spieler 2 gewinnt!");
				return field[1];
			}

			if (field[2] + field[5] + field[8] == 'O') { // Spalte

				System.out.println("Spieler 1 gewinnt!");
				return field[2];
			}
			if (field[0] + field[4] + field[8] == 'O') { // Diagonale
				System.out.println("Spieler 2 gewinnt");
				return field[0];
			}

			if (field[2] + field[4] + field[6] == 'O') { // Diagonale
				System.out.println("Spieler 2 gewinnt");
				return field[2];
			}
		}
		return 0;
	}

	
	/*
	 * public void showGrid() { JFrame grid = new JFrame("TicTacToe");
	 * grid.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); grid.setSize(400,
	 * 400); grid.setLayout(new GridLayout(3, 3)); for(int i = 0; i <
	 * Felder.length ; i++){ grid.add(new JButton("") ); }
	 * 
	 * grid.setVisible(true);
	 * 
	 * }
	 * 
	 * @Override public void actionPerformed(ActionEvent e) { for (int i = 0; i
	 * < field.length;i++){ for(int j = 0; j< field[i].length;j++){
	 * if(field[i][j] == 1){ setEnabled(true); }
	 * 
	 * } }
	 * 
	 * }
	 */

	public static void main(String[] args) {

		TicTacToe ttt = new TicTacToe();
		ttt.fillGrid();
		System.out.println(ttt.toString());
		// ttt.win();
		// ttt.showGrid();
		ttt.Steinelegen();

	}

}
